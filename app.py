import os
import sys
from PySide2.QtWidgets import (
    QAction,
    QApplication,
    QComboBox,
    QGroupBox,
    QHBoxLayout,
    QLabel,
    QMainWindow,
    QMessageBox,
    QPushButton,
    QVBoxLayout,
    QWidget,
    QSystemTrayIcon,
    QMenu,
)
from PySide2.QtCore import QSize, QRect, Qt
from PySide2.QtGui import QPixmap, QIcon
from time import sleep

from commands import (
    check_connection,
    check_expressvpn,
    connect_command,
    disconnect_command,
    get_status_short,
    get_location_key,
    get_locations_list,
    get_protocol_list,
    get_preferences_dict,
    is_activated,
    is_connected,
    set_network_lock,
    set_protocol,
)

DIR = os.path.dirname(os.path.abspath(__file__))
ICON = os.path.join(DIR, "icon.png")
ICON_ON = os.path.join(DIR, "icon-on.png")
LOGO = os.path.join(DIR, "logo.png")
TITLE = "ExpressVPN GUI"


class AppForm(QMainWindow):
    def __init__(self, parent=None):
        super(AppForm, self).__init__(parent)
        # Create UI elements
        self.setWindowTitle(TITLE)
        self.setFixedSize(QSize(400, 500))
        self.setWindowIcon(QIcon(ICON))
        self.connect_button = QPushButton()
        self.layout = QVBoxLayout()
        self.location_label = QLabel()
        self.location_combo = QComboBox()
        self.logo = QPixmap(LOGO)
        self.network_lock_combo = QComboBox()
        self.widget = QWidget()
        self.logo_label = QLabel(self.widget)
        self.options_group = QGroupBox()
        self.protocol_label = QLabel()
        self.protocol_combo = QComboBox()
        self.tray = QSystemTrayIcon(self)
        self.tray_menu = QMenu()
        self.tray_enabled = self.tray.isSystemTrayAvailable()
        self.tray_connect_action = QAction("Connect", self.tray_menu)
        self.tray_disconnect_action = QAction("Disconnect", self.tray_menu)
        # Configure App
        self._configure()

        if not self.tray_enabled:
            self.show()

    def _configure(self):
        preferences = get_preferences_dict()
        self._connect_button_toggle()
        self._create_horizontal_buttons()
        self.connect_button.setFixedHeight(48)
        self.network_lock_combo.setFixedHeight(32)
        self.network_lock_combo.addItems(["default", "strict", "off"])
        self.network_lock_combo.setCurrentIndex(
            self.network_lock_combo.findText(
                preferences["network_lock"], Qt.MatchFixedString
            )
        )
        self.network_lock_combo.currentTextChanged.connect(self._network_lock_change)
        self.protocol_label.setText("Protocol and network lock:")
        self.protocol_label.setAlignment(Qt.AlignCenter)
        self.protocol_combo.setFixedHeight(32)
        self.protocol_combo.addItems(get_protocol_list())
        self.protocol_combo.setCurrentIndex(
            self.protocol_combo.findText(
                preferences["preferred_protocol"], Qt.MatchFixedString
            )
        )
        self.protocol_combo.currentTextChanged.connect(self._protocol_change)
        if is_connected():
            self.network_lock_combo.setDisabled(True)
            self.protocol_combo.setDisabled(True)
        self.location_label.setText("Select location:")
        self.location_label.setAlignment(Qt.AlignCenter)
        self.location_combo.setFixedHeight(32)
        self.location_combo.addItems(get_locations_list())
        self.logo_label.setScaledContents(True)
        self.logo_label.setPixmap(self.logo)
        self.logo_label.setGeometry(QRect(60, 40, 280, 200))
        self.layout.addWidget(self.protocol_label)
        self.layout.addWidget(self.options_group)
        self.layout.addWidget(self.location_label)
        self.layout.addWidget(self.location_combo)
        self.layout.addWidget(self.connect_button)
        self.layout.setContentsMargins(40, 260, 40, 40)
        self.widget.setLayout(self.layout)
        self.setCentralWidget(self.widget)
        self._create_system_tray_icon()

    def _create_system_tray_icon(self):
        if not self.tray_enabled:
            return

        self.tray_connect_action.triggered.connect(self._connect_vpn)
        self.tray_connect_action.setVisible(False)

        self.tray_disconnect_action.triggered.connect(self._disconnect_vpn)
        self.tray_disconnect_action.setVisible(False)

        show_action = QAction("Show/Hide Dialog", self.tray_menu)
        show_action.triggered.connect(self._visibility_toggle)

        exit_action = QAction("Exit", self.tray_menu)
        exit_action.triggered.connect(self.close)

        self.tray_menu.addAction(self.tray_connect_action)
        self.tray_menu.addAction(self.tray_disconnect_action)
        self.tray_menu.addAction(show_action)
        self.tray_menu.addAction(exit_action)

        self.tray.activated.connect(self._visibility_toggle)

        self.tray.setContextMenu(self.tray_menu)

        self._tray_icon_reset()

        self.tray.show()

    def _tray_icon_reset(self):
        if not self.tray_enabled:
            return

        if is_connected():
            self.tray.setIcon(QIcon(ICON_ON))
            self.tray_connect_action.setVisible(False)
            self.tray_disconnect_action.setVisible(True)
        else:
            self.tray.setIcon(QIcon(ICON))
            self.tray_connect_action.setVisible(True)
            self.tray_disconnect_action.setVisible(False)

        self.tray.setToolTip(get_status_short())

    def _create_horizontal_buttons(self):
        layout = QHBoxLayout()
        layout.addWidget(self.protocol_combo)
        layout.addWidget(self.network_lock_combo)
        layout.setContentsMargins(0, 0, 0, 0)
        self.options_group.setLayout(layout)
        self.options_group.setFlat(True)

    def _network_lock_change(self):
        set_network_lock(self.network_lock_combo.currentText())

    def _protocol_change(self):
        set_protocol(self.protocol_combo.currentText())

    def _connect_button_toggle(self):
        if is_connected():
            self.connect_button.setText("Disconnect")
            try:
                self.connect_button.clicked.disconnect(self._connect_vpn)
            except RuntimeError:
                pass
            self.connect_button.clicked.connect(self._disconnect_vpn)
        else:
            self.connect_button.setText("Connect")
            try:
                self.connect_button.clicked.disconnect(self._disconnect_vpn)
            except RuntimeError:
                pass
            self.connect_button.clicked.connect(self._connect_vpn)

    def _connect_vpn(self):
        location = self.location_combo.currentText()
        self.connect_button.setText("Connecting...")
        self.connect_button.setDisabled(True)
        self.network_lock_combo.setDisabled(True)
        self.protocol_combo.setDisabled(True)
        self.location_combo.setDisabled(True)
        self.connect_button.repaint()
        self.network_lock_combo.repaint()
        self.protocol_combo.repaint()
        self.location_combo.repaint()
        connect_command(get_location_key(location))

        while not is_connected():
            sleep(1)

        self._connect_button_toggle()
        self._tray_icon_reset()
        self.connect_button.setDisabled(False)

    def _disconnect_vpn(self):
        self.connect_button.setText("Disconnecting...")
        self.connect_button.setDisabled(True)
        self.connect_button.repaint()
        disconnect_command()

        while is_connected():
            sleep(1)

        self._connect_button_toggle()
        self._tray_icon_reset()
        self.connect_button.setDisabled(False)
        self.network_lock_combo.setDisabled(False)
        self.protocol_combo.setDisabled(False)
        self.location_combo.setDisabled(False)

    def _visibility_toggle(self):
        self.setVisible((not self.isVisible()))

    def closeEvent(self, event):
        if event.spontaneous() and self.tray_enabled:
            self.hide()
            event.ignore()
            return

        event.accept()


def _check_requirements():
    box = QMessageBox()
    box.setWindowTitle(TITLE)
    box.setWindowIcon(QIcon(ICON))

    if not check_connection():
        box.setText("Please check your internet connection")
        return box

    if not check_expressvpn():
        box.setText("Please install expressvpn in order to use GUI")
        return box

    if not is_activated():
        box.setText("Please activate expressvpn in order to use GUI")
        return box

    return


if __name__ == "__main__":
    # Create the Qt Application
    app = QApplication(sys.argv)
    msg = _check_requirements()

    if msg:
        msg.show()
        result = msg.exec_()
        exit()

    form = AppForm()
    # Run the main Qt loop
    sys.exit(app.exec_())
